package copy

import (
	"fmt"
	"gin-gonic-agung-setiawan/books"
	"gin-gonic-agung-setiawan/handlers"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka_api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db Connection error:", err)
	}

	db.AutoMigrate(books.Book{})
	// booksRepository := books.NewRepository(db)

	// PART : Add Data
	// book := books.Book{}
	// book.Title = "Man Tiger"
	// book.Price = 80000
	// book.Discount = 10
	// book.Rating = 5
	// book.Description = "Ini adalah buku yang sangat bagus"

	// PART : karena udah ada err jadi gak usah pake :=. tinggal timpa aja
	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("===========================")
	// 	fmt.Println("Error Createing Book record")
	// 	fmt.Println("===========================")
	// }
	// db.debug gunanya biar kelihatan querynya apa

	// PART : find all
	// var books []books.Book
	// err = db.Debug().Find(&books).Error

	// PART : where by, kalo ambil satu pake first, kalo ambil banyak pake find
	// var books []books.Book
	// err = db.Debug().Where("title = ?", "Man Tiger").First(&books).Error
	// err = db.Debug().Where("rating = ?", 5).Find(&books).Error

	// update
	var book books.Book

	err = db.Debug().Where("id = ?", 1).First(&book).Error

	// PART : DELETE
	// err = db.Delete(&book).Error

	// find first
	// var book books.Book
	// err = db.Debug().First(&book).Error
	// Find One by id / primary key
	// err = db.Debug().First(&book, 2).Error
	// err = db.Debug().Last(&book).Error
	if err != nil {
		fmt.Println("=========================")
		fmt.Println("Error Finding Book record")
		fmt.Println("=========================")
	}

	// for _, b := range books {
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Println("book Object : ", b)
	// }

	book.Title = "Man Tiger (Revised Edition)"
	err = db.Save(&book).Error
	if err != nil {
		fmt.Println("=========================")
		fmt.Println("Error Updating Book record")
		fmt.Println("=========================")
	}

	// Repository
	// bookRepository := books.NewRepository(db)
	// allBooks, err := bookRepository.FindAll()
	// findByOne, err := bookRepository.FindByID(2)
	// fmt.Println(findByOne)

	// book := books.Book{
	// 	Title:       "$100 Starttup",
	// 	Description: "Good book",
	// 	Price:       950000,
	// 	Rating:      4,
	// 	Discount:    0,
	// }

	bookRepository.Create(book)

	for _, book := range allBooks {
		fmt.Println(book)
	}

	// find All
	// allBooks, err := bookService.FindAll()
	// findByOne, err := bookService.FindByID(2)
	// fmt.Println(findByOne)

	bookRequest := books.BookRequest{
		Title: "Gundam",
		Price: "950000",
	}

	bookService.Create(bookRequest)

	router := gin.Default()

	v1 := router.Group("v1")

	v1.GET("/", handlers.RootHandler)
	v1.GET("/hello", handlers.HelloHandler)
	v1.GET("/books/:id/:title", handlers.BooksHandler)
	v1.GET("/query", handlers.QueryHandler)
	v1.POST("/books", handlers.PostBooksHandler)

	// v2 := router.Group("v2")

	router.Run(":8888")
}
