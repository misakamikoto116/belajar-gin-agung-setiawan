package main

import (
	"gin-gonic-agung-setiawan/books"
	"gin-gonic-agung-setiawan/handlers"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka_api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db Connection error:", err)
	}

	db.AutoMigrate(books.Book{})

	// Contoh Pake 1 service tapi pake repo yang lain, asal semua interfacenya sama
	// bookFileRepository := books.NewFileRepository()
	// bookService := books.NewService(bookRepository)

	bookRepository := books.NewRepository(db)
	bookService := books.NewService(bookRepository)
	bookHandler := handlers.NewBookHandler(bookService)

	router := gin.Default()

	v1 := router.Group("v1")

	v1.POST("/books", bookHandler.PostBooksHandler)
	v1.GET("/books", bookHandler.GetBooks)
	// v2 := router.Group("v2")

	router.Run(":8888")
}

// endpoint belajar
// v1.GET("/", bookHandler.RootHandler)
// v1.GET("/hello", bookHandler.HelloHandler)
// v1.GET("/books/:id/:title", bookHandler.BooksHandler)
// v1.GET("/query", bookHandler.QueryHandler)
