package books

import "encoding/json"

type BookRequest struct {
	Title       string      `binding:"required"`
	Price       json.Number `binding:"required,number"`
	SubTitle    string      `json:"sub_title"`
	Description string      `json:"description" binding:"required"`
	Rating      json.Number `binding:"required,number"`
	Discount    json.Number `binding:"required,number"`
}
