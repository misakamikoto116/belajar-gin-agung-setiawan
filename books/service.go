package books

type Service interface {
	FindAll() ([]Book, error)
	FindByID(ID int) (Book, error)
	Create(book BookRequest) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) FindByID(ID int) (Book, error) {
	book, err := s.repository.FindByID(ID)
	return book, err
}

// Big int = int64, int = int32
func (s *service) Create(request BookRequest) (Book, error) {
	price, _ := request.Price.Int64()
	rating, _ := request.Rating.Int64()
	discount, _ := request.Discount.Int64()

	book := Book{
		Title:       request.Title,
		Price:       int(price),
		Description: request.Description,
		Rating:      int(rating),
		Discount:    int(discount),
	}

	newBook, err := s.repository.Create(book)
	return newBook, err
}
