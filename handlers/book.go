package handlers

import (
	"errors"
	"fmt"
	"net/http"

	"gin-gonic-agung-setiawan/books"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type bookHandler struct {
	service books.Service
}

func NewBookHandler(service books.Service) *bookHandler {
	return &bookHandler{service}
}

func (h *bookHandler) GetBooks(c *gin.Context) {
	allBooks, err := h.service.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	var booksResponse []books.BookResponse

	for _, b := range allBooks {
		bookResponse := books.BookResponse{
			Title:       b.Title,
			Price:       b.Price,
			Description: b.Description,
			Rating:      b.Rating,
			Discount:    b.Discount,
		}

		booksResponse = append(booksResponse, bookResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
	})
}

// contoh ?title=12
func (h *bookHandler) QueryHandler(c *gin.Context) {
	title := c.Query("title")
	price := c.Query("price")

	c.JSON(http.StatusOK, gin.H{
		"title": title,
		"price": price,
	})
}

// with validation
// POST
// Kalo post harus pake struct, jadi nanti isi datanya dimasukan dalam struct dengan shouldbindjson
// struct Title dan Price pake Capitalize, tapi di postman hanya title dan price, itu automatis, dan harus Capitilize biar bisa dipanggil di func lain
// struct SubTitle akan menangkap json / body sub_title, jdi di postman pake sub_title keynya
// Di bawah err c.json ada return, itu biar gk lanjut di kode bawahnya
// %s pertama dimasukkan data e.Field, lalu %s kedua dimasukkan data e.ACtualTag
// Kalo mau pake required number gak bisa pake int tapi pake json.number, tapi meski "20" tetep bisa karena bakal auto convert
// sementara price dijadiin interface{} dlu, json Number Kelihatan tidak berfungsi
// structnya dipindah ke package books
func (h *bookHandler) PostBooksHandler(c *gin.Context) {
	var request books.BookRequest

	err := c.ShouldBindJSON(&request)
	if err != nil {

		var ve validator.ValidationErrors
		errorMessages := []string{}

		// Ubah type err ke validator.validationErrors
		if errors.As(err, &ve) {
			for _, e := range err.(validator.ValidationErrors) {
				errorMessage := fmt.Sprintf("Eror On Field %s, condition: %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)
			}
		}

		c.JSON(http.StatusBadRequest, gin.H{"errors": errorMessages})
		return
	}

	book, err := h.service.Create(request)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"errors": err})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": book,
	})
}

// ini function belajar
// ini disebut function yang dimiliki sebuah struct, bkn function biasa / sebutannya method
// func (h *bookHandler) RootHandler(c *gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"name": "Joel Effendi",
// 		"age":  21,
// 		"bio":  "A Software Engineer",
// 	})
// }

// func (h *bookHandler) HelloHandler(c *gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"title": "Hello World",
// 		"desc":  "Belajar Golang Bareng Agung Setiawan",
// 	})
// }

// func (h *bookHandler) BooksHandler(c *gin.Context) {
// 	id := c.Param("id")
// 	title := c.Param("title")

// 	c.JSON(http.StatusOK, gin.H{
// 		"id":    id,
// 		"title": title,
// 	})
// }
